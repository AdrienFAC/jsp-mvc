/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author pedago
 */
@WebServlet(urlPatterns = {"/Controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	try {
                DAO dao = new DAO(DataSourceFactory.getDataSource());
		String paramCode = request.getParameter("code");
		String paramTaux = request.getParameter("taux");
		String paramAction = request.getParameter("action");
                String message = null;
                
                if (paramCode != null && paramCode.length() > 1)
                {
                    message = "Le code ne peut contenir qu'un seul caractère";
                }
                else if (paramCode != null && paramTaux != null && paramAction != null && paramAction.equals("ADD"))
                {
                    dao.addDiscountCode(paramCode, Float.valueOf(paramTaux));
                    message = "Code " + paramCode + " ajouté";
                }
                else if (paramAction != null && paramAction.equals("DELETE") && paramCode != null)
                {
                    dao.deleteDiscountCode(paramCode);
                    message = "Code " + paramCode + " supprimé";
                }
                    

                List<Discount> lstDiscountCode = dao.getDiscountCodes();

                request.setAttribute("lstDiscountCode", lstDiscountCode);
                request.setAttribute("message", message);
		request.getRequestDispatcher("DiscountView.jsp").forward(request, response);
	} catch (Exception e) {
            System.out.println("Erreur " + e.getMessage());
	}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
