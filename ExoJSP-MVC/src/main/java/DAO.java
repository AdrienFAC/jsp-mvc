/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
/**
 *
 * @author pedago
 */
public class DAO {
    
    	protected final DataSource myDataSource;

	public DAO(DataSource dataSource) {
		this.myDataSource = dataSource;
	}
                
        public List<Discount> getDiscountCodes() throws DAOException {
		List<Discount> result = new LinkedList<>();
		String sql = "SELECT * FROM APP.DISCOUNT_CODE";
		try (Connection connection = myDataSource.getConnection();
			PreparedStatement stmt = connection.prepareStatement(sql)) {
			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					String name = rs.getString("DISCOUNT_CODE");
					float rate = rs.getFloat("RATE");
					Discount c = new Discount(name, rate);
					result.add(c);
				}
			}
		}  catch (SQLException ex) {
			Logger.getLogger("DAO").log(Level.SEVERE, null, ex);
			throw new DAOException(ex.getMessage());
		}
		return result;
	}
        
        
        public int deleteDiscountCode(String discountName) throws DAOException {
		String sql = "DELETE FROM DISCOUNT_CODE WHERE DISCOUNT_CODE = ?";
		try (   Connection connection = myDataSource.getConnection();
			PreparedStatement stmt = connection.prepareStatement(sql)
                ) {
			stmt.setString(1, discountName);
			return stmt.executeUpdate();
		}  catch (SQLException ex) {
			Logger.getLogger("DAO").log(Level.SEVERE, null, ex);
			throw new DAOException(ex.getMessage());
		}
        }
        
        public void addDiscountCode(String name, float rate) throws DAOException {
            String sql = "INSERT INTO DISCOUNT_CODE VALUES(?, ?)";
            try ( Connection connection = myDataSource.getConnection();
                  PreparedStatement stmt = connection.prepareStatement(sql)
            ) {
		stmt.setString(1, name);
		stmt.setFloat(2, rate);
                stmt.executeUpdate();
            }  catch (SQLException ex) {
                    Logger.getLogger("DAO").log(Level.SEVERE, null, ex);
                    throw new DAOException(ex.getMessage());
            }
        }
}
