/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pedago
 */
public class Discount {
    private String name;
    private float rate;
    
    public Discount(String name, float rate){
        this.name = name;
        this.rate = rate;
    }
    
    public String getName(){
        return this.name;
    }
    
    public float getRate(){
        return this.rate;
    }
}
