<%-- 
    Document   : DiscountView
    Created on : 13 nov. 2018, 16:05:53
    Author     : pedago
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Import des "tag libraries" de JSP -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSTL Custom Tags</title>
    </head>
    <body>
        <form method="get">
            <fieldset>
                <label for="code">Code: </label>
                <input type="hidden" id="i_hidden" name="action" value="ADD"/>
                <input type="text" id="code" name="code" value="" size="20" maxlength="60" />
                <br />
                <label for="taux">Taux: </label>
                <input type="text" id="taux" name="taux" value="" size="20" maxlength="20" />
                <br />
                <input type="submit" value="Ajouter" class="sansLabel" />
                <br />
            </fieldset>
        </form>

        ${message}<hr>

        <table style="width:100%">
            <tr>
              <th>Code</th>
              <th>Taux</th> 
              <th>Action</th>
            </tr>
            <c:forEach var="discountCode" items="${lstDiscountCode}" varStatus="status">
                <tr><td>${discountCode.name}</td><td>${discountCode.rate}</td><td><a href="?action=DELETE&code=${discountCode.name}">delete</a></td></tr>
            </c:forEach>
        </table>
    </body>
</html>
